# Diaphysator 1.1.4 (Release date: 2019-05-13)

* Updated references in documentation files
* Updated LICENSE (GPL-3 -> CeCILL-2)


# Diaphysator 1.1.3 (Release date: 2019-04-08)

* New contributor
* Updated UI and README with banner and logo


# Diaphysator 1.1.2 (Release date: 2019-04-03)

## Minor changes

* Updated documentation files, and citation info added.
* Updated NAMESPACE file to load only the useful functions.


# Diaphysator 1.1.1 (Release date: 2019-03-18)

## Minor improvement

* Tab "Cortical thickness: correlation coefficient": new radio buttons added that allow to display either raw coefficient values, or absolute values.

## Other minor changes

* Updated help button, and update references in documentation files, to indicate that example data files are now available on Zenodo.
* The unit used to plot the thickness maps (millimeter) is now given in tab "Cortical thickness: mean".


# Diaphysator 1.1.0 (Release date: 2019-03-04)

## Major improvements

* The color scales used in all four tabs are now suitable for colorblind users. Consequently, Diaphysator a new package dependency (RColorBrewer) is added.
* Tab "Cortical thickness: correlation coefficient": a non-symmetric color scale is now chosen, in order to distinguish positive and negative correlations.
* Tab "CSG: correlations": new checkbox added that allows to display either raw coefficient values, or absolute values.

## Minor improvements

* Added icons on action buttons ("Help" and "Load data files") for better readability.
* The UI parameter "Increase font size" now also increases the size of contours labels for "Cortical thickness: mean" and "Cortical thickness: correlations" tabs, and not only label axes as previously.
* Various code improvements that improves the readability of the R code. More comments added in almost all R functions.
* Updated README


# Diaphysator 1.0.2 (Release date: 2018-10-30)

## Bug fix

* The horizontal axis on all plots now respects in all cases the orientation provided by the source files from the Extractor.


# Diaphysator 1.0.1 (Release date: 2018-10-18)

* Updated y-label on all correlation plots.


# Diaphysator 1.0.0 (Release date: 2018-10-18)

First public release on GitLab.
