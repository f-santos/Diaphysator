<img src="/inst/DiaphysatorShinyApp/www/banner_Diaphysator.png" height="125">

## Using Diaphysator online

Diaphysator can be used simply as an online application:
    https://diaphysator.shinyapps.io/maps/

This can be easier for users who are not familiar with R, but Diaphysator will be slightly slower.

## Using Diaphysator as an R package

### Install prerequisites

1. Install the R package `devtools` by typing the following command line into the R console:

		install.packages("devtools")

2. Install build environment
    * **Windows:** Install latest version of *[Rtools](https://cran.r-project.org/bin/windows/Rtools/)*. During the installation process, make sure to select *"Edit the system path"*.
    * **OSX:** Install *[XCODE](https://developer.apple.com/xcode/)*

### Install Diaphysator

Run the following commands in R:
        
	library(devtools)
	install_git('https://gitlab.com/f-santos/Diaphysator.git')

### Run Diaphysator

To start the graphical interface, run the following commands into the R console:

	library(Diaphysator)
	StartDiaphysator()
	

### Update Diaphysator

To update Diaphysator if a new version is available on GitLab, simply install it again:

	install_git('https://gitlab.com/f-santos/Diaphysator.git')
	

## Citation

The users of Diaphysator who are willing to cite the package in a scientific article can find citation information by typing:

	citation("Diaphysator")

into the R console, after installing the package.

## Links

For further documentation, a video tutorial and a technical documentation are available on [Diaphysator webpage](http://projets.pacea.u-bordeaux.fr/logiciel/extractor/extractor_diaphysator.html).
